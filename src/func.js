const getSum = (str1, str2) => {
  
  
  if (typeof(str1) !== 'string' || typeof(str2) !== 'string' 
  || str1 !== str1.replace(/[^\d]/g, '') || str2 !== str2.replace(/[^\d]/g, '')){
    return false;
  }else {
    if (str1.length === 0){
      str1 = '0';
    }
    if (str2.length === 0){
      str2 = '0';
    }
    return (Number.parseInt(str1) + Number.parseInt(str2)).toString();
  }
  
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  let postCount = 0;
  let comCount = 0;
  for (const post of listOfPosts) {
    if (post.author === authorName){
      postCount++;
    }
    if (post.hasOwnProperty('comments')){
      for (const com of post.comments) {
        if (com.author === authorName){
          comCount++;
        }
      }
    }
  }
  return "Post:" + postCount + ",comments:" + comCount;
};

const tickets=(people)=> {
  let curSum = 0;
  let cost = 25;
  for (const e of people) {
    if (Number.parseInt(e) !== cost){
      if (people.indexOf(e) !== 0 && Number.parseInt(e) <= curSum + cost){
        curSum += cost;
      }else {
        return "NO";
      }
    }else{
      curSum +=cost;
    }
  }
  return "YES";
};


module.exports = {getSum, getQuantityPostsByAuthor, tickets};
